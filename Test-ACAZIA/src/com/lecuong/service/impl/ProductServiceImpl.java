package com.lecuong.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.lecuong.entity.Category;
import com.lecuong.entity.Product;
import com.lecuong.repository.ProductRepository;
import com.lecuong.service.ProductService;

public class ProductServiceImpl implements ProductService {
	
	private final ProductRepository productRepository;

	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}
	
	@Override
	public void sortProductByPrice(String searchCategoryTag) {
		
		List<Product> listProducts = productRepository.findAll();
		
		List<Product> listSortProducts = new ArrayList<Product>();

		for (Product ele : listProducts) {
			if (searchCategoryTag.equalsIgnoreCase(ele.getCategoryTag())) {

				listSortProducts.add(ele);

				Collections.sort(listSortProducts, new Comparator<Product>() {
					@Override
					public int compare(Product o1, Product o2) {
						return (o1.getPrice() < o2.getPrice()) ? 1 : -1;
					}
				});
			}
		}

		listSortProducts.forEach(p -> System.out.println(p));
	}

	@Override
	public void sortProductByName(String searchCategoryTag) {
		
		List<Product> listProducts = productRepository.findAll();
		
		List<Product> listSortProducts = new ArrayList<Product>();

		for (Product ele : listProducts) {
			if (searchCategoryTag.equalsIgnoreCase(ele.getCategoryTag())) {

				listSortProducts.add(ele);

				Collections.sort(listSortProducts, new Comparator<Product>() {
					@Override
					public int compare(Product o1, Product o2) {
						return o1.getName().compareTo(o2.getName());
					}
				});
			}
		}

		listSortProducts.forEach(p -> System.out.println(p));
	}

}
